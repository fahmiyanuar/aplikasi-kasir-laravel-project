
<div class="modal-header">
    <h5 class="modal-title" id="exampleModalLabel">Tambah Transaksi Pembelian</h5>
    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">×</span>
    </button>
</div>
<form action="javascript:void(0)" role="form" method="POST" id="form">
    <div class="modal-body">
        {{ csrf_field() }}
        <table class="table" id="transaksi">
            <thead>
                <tr>
                    <th scope="col" style="width: 5%">#</th>
                    <th scope="col" style="width: 40%">Nama Barang</th>
                    <th scope="col" style="width: 10%">Jumlah</th>
                    <th scope="col" style="width: 20%">Harga</th>
                    <th scope="col" style="width: 20%">Subtotal</th>
                    <th scope="col"><button type="button" class="btn btn-success btn-sm add-row"><i class="fas fa-plus"></i></button></th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>1</td>
                    <td>
                        <select name="barang[]" id="barang" class="form-control barang">
                            <option></option>
                            @foreach ($data as $barang)
                            <option value="{{ $barang->id }}">{{ $barang->nama_barang }}</option>
                            @endforeach
                        </select>
                    </td>
                    <td><input type="number" name="jumlah[]" min="1" step="1" class="form-control jumlah" value="1" /></td>
                    <td><input type="number" name="harga[]" class="form-control harga" readonly /></td>
                    <td><input type="number" class="form-control subtotal" readonly /></td>
                    <td>&nbsp;</td>
                </tr>
            </tbody>
            <tfoot>
                <tr>
                    <th colspan="4" style="text-align: right">Total</th>
                    <th><input type="number" name="total" class="form-control total" readonly /></th>
                </tr>
            </tfoot>
        </table>
    </div>
    <div class="modal-footer">
        <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
        <button class="btn btn-primary" type="submit" id="submit">Simpan</button>
    </div>
</form>
    <script type="text/javascript">

    function addRowTable() {
        var x = 1;
        $('.add-row').click(function(e) {
            e.preventDefault();
            x++;
            $('#transaksi').append(
                '<tr><td>' + x + '</td>' +
                '<td><select name="barang[]" id="barang" class="form-control barang">' +
                '<option></option>' + 
                @foreach ($data as $barang)
                        '<option value="{{ $barang->id }}">{{ $barang->nama_barang }}</option>' +
                        @endforeach
                '</select></td>' +
                '<td><input type="number" name="jumlah[]" min="1" step="1" class="form-control jumlah" value="1" /></td>' +
                '<td><input type="number" name="harga[]" class="form-control harga" readonly /></td>' +
                '<td><input type="number" class="form-control subtotal" readonly /></td>' +
                '<td><button type="button" class="btn btn-danger btn-sm remove-row"><i class="far fa-trash-alt"></i></button></td>' +
                '</tr>'
            );
            renderSelect2();
            bindJumlah();
        });

        $("#transaksi").on("click",".remove-row", function(e) {
            e.preventDefault(); $(this).parent().parent().remove(); 
            x--;
            hitungTotal();
        });

    }

    function renderSelect2() {
        $('.barang').select2({
            dropdownParent: $("#myModal")
        }).on('change', function() {
            var barang = $(this);
            $.ajax({
                type:'GET',
                url:"{{ url('transaksi/getharga') }}",
                data:'id=' + $(barang).val(),
                success:function(response) {
                    $(barang).parent().parent().find('.harga').val(response);
                    hitungTotal();
                }
            });
        });
    }

    function hitungTotal() {
        var sum = 0;
        var total = 0;
        $('tbody tr').each(function() {
            var jumlah = $(this).find('.jumlah').val();
            var harga = $(this).find('.harga').val();
            
            if (!isNaN(harga) && harga.length !== 0) {
                sum = parseInt(jumlah) * parseInt(harga);
            }
			
			total += sum;
            $(this).find('.subtotal').val(sum);
        });
		$('.total').val(total);
    }

    function bindJumlah() {
        $('.jumlah')
            .change(hitungTotal)
            .keyup(hitungTotal)
            .keypress(hitungTotal)
            .focus(hitungTotal)
            .blur(hitungTotal)
    }

    $(document).ready(function() {

        addRowTable();
        bindJumlah();

        const numInputs = document.querySelectorAll('input[type=number]');
        numInputs.forEach(function(input) {
            input.addEventListener('change', function(e) {
                if (e.target.value == '') {
                e.target.value = 1
                }
            });
        });

       renderSelect2();

        $('#submit').click(function(e) {
            e.preventDefault();
            
            var myForm = $('#form').serialize();
            swal({
                title: "Apakah Anda yakin?",
                text: "Apakah Anda yakin akan menyimpan data ini?",
                icon: "warning",
                buttons: true,
            }).then((posting) => {
                    $.ajax({
                    type:'POST',
                    url:"{{ route('transaksi.post') }}",
                    data: myForm,
                    success:function(response){
                        if(response.status == 'error') {
                            swal({
                                title: response.title,
                                text: response.message,
                                icon: response.status
                            }).then(() => {
                                window.location.href = "{{ url('/transaksi') }}";
                            });
                        }else{
                            swal({
                                title: 'Berhasil tambah data',
                                text: "Apakah Anda akan mencetak struk transaksi ini?",
                                icon: 'success',
                                dangerMode: true,
                                buttons: ['Tidak', 'Ya, Print'],
                            }).then(result => {
                                if(result) {
                                    window.open("{{ url('transaksi/print') }}" + "/" + response.id, '_blank');
                                    window.location.href = "{{ url('/transaksi') }}";
                                } else {
                                    window.location.href = "{{ url('/transaksi') }}";
                                }
                            });
                        }
                    }
                });
            });
        });

        function printErrorMsg (msg) {
            $.each( msg, function( key, value ) {
            //console.log(key);
              $('.'+key+'_err').text(value);
            });
        }
    });
    </script>
