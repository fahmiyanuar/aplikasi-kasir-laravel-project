
<div class="modal-header">
    <h5 class="modal-title" id="exampleModalLabel">Hapus Produk</h5>
    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">×</span>
    </button>
</div>
<form action="javascript:void(0)" role="form" method="POST" id="form">
    <div class="modal-body">
        {{ csrf_field() }}
        <input type="hidden" name="id_barang" value="{{ $data->id }}" />
        Apakah Anda yakin akan menghapus barang "{{ $data->nama_barang }}" ?
    </div>
    <div class="modal-footer">
        <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
        <button class="btn btn-danger" type="submit" id="submit">Delete</button>
    </div>
</form>
    <script type="text/javascript">
    $(document).ready(function() {
        $('#submit').click(function(e) {
            e.preventDefault();
            
            var myForm = $('#form').serialize();
            $.ajax({
                type:'POST',
                url:"{{ route('produk.delete', ['id' => $data->id]) }}",
                data: myForm,
                success:function(response){
                    if($.isEmptyObject(response.error)){
                        swal({
                            title: 'Sukses!',
                            text: response.success,
                            icon: 'success'
                        }).then(() => {
                            window.location.href = "{{ url('/manage/produk') }}";
                        });
                        //alert(response.success);
                    }
                }
            });
        });
    });
    </script>
