
<div class="modal-header">
    <h5 class="modal-title" id="exampleModalLabel">Detail Transaksi Pembelian ID {{ $dataTransaksi->id }}</h5>
    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">×</span>
    </button>
</div>
    <div class="modal-body">
        <div class="row">
            <div class="col">Tanggal Transaksi :</div>
            <div class="col">{{ \Carbon\Carbon::parse($dataTransaksi->created_at)->translatedFormat('l, d F Y H:i') }}</div>
        </div>
        <table class="table mt-3">
            <thead>
                <tr>
                    <th scope="col" style="width: 5%">#</th>
                    <th scope="col" style="width: 40%">Nama Barang</th>
                    <th scope="col" style="width: 10%">Jumlah</th>
                    <th scope="col" style="width: 20%">Harga</th>
                    <th scope="col" style="width: 20%">Subtotal</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($dataTransaksiBarang as $resultBarang)
                <tr>
                    <td>{{ ++$no }}</td>
                    <td>{{ $resultBarang->nama_barang->nama_barang }}</td>
                    <td>{{ $resultBarang->jumlah }}</td>
                    <td style="text-align: right">Rp. {{ number_format($resultBarang->harga_satuan) }}</td>
                    <td style="text-align: right">Rp. {{ number_format($resultBarang->jumlah * $resultBarang->harga_satuan) }}</td>
                </tr>
                @endforeach
            </tbody>
            <tfoot>
                <tr>
                    <th colspan="4">Total</th>
                    <th style="text-align: right">Rp. {{ number_format($dataTransaksi->total_harga) }}</th>
                </tr>
            </tfoot>
        </table>
    </div>
    <div class="modal-footer">
        <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
        <button class="btn btn-danger" type="button" id="export">Save to PDF</button>
    </div>
    <script type="text/javascript">

    $(document).ready(function() {
        $('#export').click(function() {
            window.open("{{ url('/transaksi/download') .'/'.$dataTransaksi->id }}", '_blank');
        });
    });
    </script>
