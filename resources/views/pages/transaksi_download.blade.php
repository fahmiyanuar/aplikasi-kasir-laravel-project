<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Download Detail Transaksi ID {{ $dataTransaksi->id }}</title>
    <style>
        #download {
            font-family: Arial, Helvetica, sans-serif;
            border-collapse: collapse;
            width: 100%;
        }

        #download td, #download th {
            border: 1px solid #000;
            padding: 8px;
        }

        #download tr:nth-child(even) {
            background-color: #EEE;
        }

        #download th {
            padding-top: 12px;
            padding-bottom: 12px;
            text-align: left;
            background-color: darkgray
        }
    </style>
</head>
<body style="font-size: 12pt">
    <div style="text-align: center"><h1>Detail Transaksi ID #{{ $dataTransaksi->id }}</h1></div>
    <div style="text-align: center"><h3>{{ \Carbon\Carbon::parse($dataTransaksi->created_at)->translatedFormat('l, d F Y H:i') }}</h3></div>
    <table id="download">
        <thead>
            <tr>
                <th style="width: 5%">No</th>
                <th style="width: 40%">Nama Barang</th>
                <th style="width: 10%">Jumlah</th>
                <th style="width: 20%">Harga</th>
                <th style="width: 20%">Subtotal</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($dataTransaksiBarang as $resultBarang)
            <tr>
                <td>{{ ++$no }}</td>
                <td>{{ $resultBarang->nama_barang->nama_barang }}</td>
                <td>{{ $resultBarang->jumlah }}</td>
                <td style="text-align: right">Rp. {{ number_format($resultBarang->harga_satuan) }}</td>
                <td style="text-align: right">Rp. {{ number_format($resultBarang->jumlah * $resultBarang->harga_satuan) }}</td>
            </tr>
            @endforeach
        </tbody>
        <tfoot>
            <tr>
                <th colspan="4">Total</th>
                <th style="text-align: right">Rp. {{ number_format($dataTransaksi->total_harga) }}</th>
            </tr>
        </tfoot>
    </table>
</body>
</html>