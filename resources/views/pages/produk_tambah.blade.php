
<div class="modal-header">
    <h5 class="modal-title" id="exampleModalLabel">Tambah Produk</h5>
    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">×</span>
    </button>
</div>
<form action="javascript:void(0)" role="form" method="POST" id="form">
    <div class="modal-body">
            {{ csrf_field() }}
            <div class="form-group">
                <label for="nama_barang">Nama Barang <span class="text-danger">*</span></label>
                <input type="text" name="nama_barang" id="nama_barang" class="form-control" placeholder="Input Nama Barang" />
                <span class="text-danger error-text nama_barang_err"></span>
            </div>
            <div class="form-group">
                <label for="harga_satuan">Harga Satuan <span class="text-danger">*</span></label>
                <input type="number" name="harga_satuan" id="harga_satuan" class="form-control" placeholder="Input Harga Satuan" />
                <span class="text-danger error-text harga_satuan_err"></span>
            </div>
    </div>
    <div class="modal-footer">
        <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
        <button class="btn btn-primary" type="submit" id="submit">Simpan</button>
    </div>
</form>
    <script type="text/javascript">
    $(document).ready(function() {
        $('#submit').click(function(e) {
            e.preventDefault();
            
            var myForm = $('#form').serialize();
            $.ajax({
                type:'POST',
                url:"{{ route('produk.post') }}",
                data: myForm,
                success:function(response){
                    if($.isEmptyObject(response.error)){
                        swal({
                            title: 'Sukses!',
                            text: response.success,
                            icon: 'success'
                        }).then(() => {
                            window.location.href = "{{ url('/manage/produk') }}";
                        });
                        //alert(response.success);
                    }else{
                        printErrorMsg(response.error);
                    }
                }
            });
        });

        function printErrorMsg (msg) {
            $.each( msg, function( key, value ) {
            //console.log(key);
              $('.'+key+'_err').text(value);
            });
        }
    });
    </script>
