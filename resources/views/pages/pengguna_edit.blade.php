
<div class="modal-header">
    <h5 class="modal-title" id="exampleModalLabel">Edit Pengguna</h5>
    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">×</span>
    </button>
</div>
<form action="javascript:void(0)" role="form" method="POST" id="form">
    <div class="modal-body">
            {{ csrf_field() }}
            <div class="form-group">
                <label for="nama">Nama <span class="text-danger">*</span></label>
                <input type="text" name="nama" id="nama" class="form-control" placeholder="Input Nama" value="{{ $data->nama }}" />
                <span class="text-danger error-text nama_err"></span>
            </div>
            <div class="form-group">
                <label for="username">Username <span class="text-danger">*</span></label>
                <input type="text" name="username" id="username" class="form-control" placeholder="Input Username" value="{{ $data->username }}" />
                <span class="text-danger error-text username_err"></span>
            </div>
            <div class="form-group">
                <label for="email">Email <span class="text-danger">*</span></label>
                <input type="email" name="email" id="email" class="form-control" placeholder="Input Email" value="{{ $data->email }}" />
                <span class="text-danger error-text email_err"></span>
            </div>
            <div class="form-group">
                <label for="password">Password</label>
                <input type="text" name="password" id="password" class="form-control" placeholder="Input Password" />
                <small id="passwordHelpBlock" class="form-text text-muted">
                    Kosongkan password jika tidak ada perubahan password
                </small>
                <span class="text-danger error-text password_err"></span>
            </div>
            <div class="form-group">
                <label for="role_user">Role <span class="text-danger">*</span></label>
                <select name="role_user" class="form-control" id="role_user">
                    <option></option>
                    <option value="Admin" @if ($data->role == 'Admin') selected @endif>Admin</option>
                    <option value="Kasir" @if ($data->role == 'Kasir') selected @endif>Kasir</option>
                </select>
                <span class="text-danger error-text role_user_err"></span>
            </div>
    </div>
    <div class="modal-footer">
        <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
        <button class="btn btn-primary" type="submit" id="submit">Simpan</button>
    </div>
</form>
    <script type="text/javascript">
    $(document).ready(function() {
        $('#submit').click(function(e) {
            e.preventDefault();
            $('.error-text').text('');
            
            var myForm = $('#form').serialize();
            $.ajax({
                type:'POST',
                url:"{{ route('pengguna.update', ['id' => $data->id]) }}",
                data: myForm,
                success:function(response){
                    if($.isEmptyObject(response.error)){
                        swal({
                            title: 'Sukses!',
                            text: response.success,
                            icon: 'success'
                        }).then(() => {
                            window.location.href = "{{ url('/manage/pengguna') }}";
                        });
                        //alert(response.success);
                    }else{
                        printErrorMsg(response.error);
                    }
                }
            });
        });

        function printErrorMsg (msg) {
            $.each( msg, function( key, value ) {
            //console.log(key);
              $('.'+key+'_err').text(value);
            });
        }
    });
    </script>
