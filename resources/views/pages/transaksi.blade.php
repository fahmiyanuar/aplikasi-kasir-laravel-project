@extends('layouts.main')

@section('title', 'Transaksi Pembelian')

@section('style')
<link href={{ asset("assets/vendor/datatables/dataTables.bootstrap4.min.css") }} rel="stylesheet">
<link href={{ asset("assets/vendor/bootstrap-datepicker/css/bootstrap-datepicker3.min.css") }} rel="stylesheet">
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.1/css/select2.min.css" rel="stylesheet" />
@endsection

@section('content')
<div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-4 text-gray-800">Transaksi Pembelian</h1>
    <div class="row">
        <div class="col">
            <div class="card shadow mb-4">
                <div class="card-header py-3">
                    <div class="row">
                        <div class="col d-flex align-items-center">
                            <h6 class="m-0 font-weight-bold text-primary justify-content-start">Data Transaksi Pembelian</h6>
                        </div>
                        <div class="col d-flex justify-content-end">
                            <button type="button" class="btn btn-sm btn-primary" id="tambah" data-toggle="modal" data-target="#myModal"><i class="fas fa-plus"></i> Tambah Data</button>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-6">
                            <button class="btn btn-primary btn-sm mb-1" type="button" data-toggle="collapse" data-target="#pencarianData" aria-expanded="false" aria-controls="pencarianData">Cari Data</button>
                            <div class="card mb-4" style="border: none">
                                <div class="collapse" id="pencarianData">
                                    <div class="card-body">
                                        <form action="/transaksi" role="form" method="POST">
                                            {{ csrf_field() }}
                                            @if (Session::get('role') == 'Admin')
                                            <div class="form-group row">
                                                <label for="tanggal" class="col-sm-4 col-form-label">Tanggal Transaksi</label>
                                                <div class="col-sm-8">
                                                    <input type="text" class="form-control" id="tanggal" name="tanggal" placeholder="Tanggal Transaksi" value="{{ Request::get('tanggal') }}">
                                                </div>
                                            </div>
                                            @endif
                                            <div class="form-group row">
                                                <label for="harga_min" class="col-sm-4 col-form-label">Total Harga</label>
                                                <div class="col-sm-4">
                                                    <input type="text" class="form-control" id="harga_min" name="harga_min" placeholder="Rp. Harga Min" value="{{ Request::get('harga_min') }}">
                                                </div>
                                                <div class="col-sm-4">
                                                    <input type="text" class="form-control" id="harga_max" name="harga_max" placeholder="Rp. Harga Max" value="{{ Request::get('harga_max') }}">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="barangs" class="col-sm-4 col-form-label">Barang</label>
                                                <div class="col-sm-8">
                                                    <select name="barangs[]" id="barangs" class="form-control" multiple style="width: 100%">
                                                        <option></option>
                                                        @foreach ($dataBarang as $barang)
                                                        <option value="{{ $barang->id }}" @if ($barang->id == Request::get('barangs')) selected @endif>{{ $barang->nama_barang }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-sm-8 offset-sm-4">
                                                    <button type="submit" class="btn btn-primary btn-sm"><i class="fas fa-search"></i> Cari</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                            <tr>
                                <th>#ID</th>
                                <th>Waktu Transaksi</th>
                                <th>Total Harga</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($data as $result)
                            <tr>
                                <td>{{ $result->id }}</td>
                                <td data-order="{{ strtotime($result->created_at) }}">{{ \Carbon\Carbon::parse($result->created_at)->translatedFormat('l, d F Y H:i') }}</td>
                                <td data-order="{{ $result->total_harga }}" style="text-align: right">Rp. {{ number_format($result->total_harga) }}</td>
                                <td><button type="button" class="btn btn-sm btn-success detail" title="Detail" data-toggle="modal" data-target="#myModal" ref="{{ $result->id }}">Detail</button></td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')

    <script src={{ asset("assets/vendor/datatables/jquery.dataTables.min.js") }}></script>
    <script src={{ asset("assets/vendor/datatables/dataTables.bootstrap4.min.js") }}></script>
    <script src={{ asset("assets/vendor/bootstrap-datepicker/js/bootstrap-datepicker.min.js") }}></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.1/js/select2.min.js"></script>

    <script type="text/javascript">
    $(document).ready(function() {
        $('#tanggal').datepicker({
            autoclose: true,
            todayHighlight: true,
            format: 'yyyy-mm-dd'
        });

        $('#barangs').select2().val([
            @if (!empty(Request::get('barangs')))
            @foreach (Request::get('barangs') as $key)
            {{ $key.',' }}
            @endforeach
            @endif
        ]).trigger('change');

        $('#tambah').click(function() {
            $('#myModal .modal-dialog').addClass('modal-xl');
            $('#myModal .modal-content').load('{{ url("/transaksi/tambah") }}');
        });
        $('.detail').click(function() {
            $('#myModal .modal-dialog').addClass('modal-xl');
            $('#myModal .modal-content').load('{{ url("/transaksi/detail") }}' + '/' + $(this).attr('ref'));
        });
        $('#dataTable').DataTable({
            "searching": false
        });
    });
    </script>
@endsection