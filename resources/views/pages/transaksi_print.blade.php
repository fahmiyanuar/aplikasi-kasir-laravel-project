<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Cetak Transaksi Pembelian ID #{{ $dataTransaksi->id }}</title>
</head>
<body style='font-family:tahoma; font-size:8pt;'>
        <center><table style='width:218px; font-size:16pt; font-family:calibri; border-collapse: collapse;' border = '0'>
        <td width='70%' align='CENTER' vertical-align:top'><span style='color:black;'>
        <b>APLIKASI KASIR</b></br>
         
         
        <span style='font-size:12pt'>No. : {{ $dataTransaksi->id }}, {{ \Carbon\Carbon::parse($dataTransaksi->created_at)->translatedFormat('d F Y H:i:s') }}</span></br>
        </td>
        </table>
        <style>
        hr { 
            display: block;
            margin-top: 0.5em;
            margin-bottom: 0.5em;
            margin-left: auto;
            margin-right: auto;
            border-style: inset;
            border-width: 1px;
        } 
        </style>
        <table cellspacing='0' cellpadding='0' style='width:218px; font-size:12pt; font-family:calibri;  border-collapse: collapse;' border='0'>
         
        <tr align='center'>
        <td width='14%'>Barang</td>
        <td width='16%'>Harga</td>
        <td width='4%'>Jumlah</td>
        <td width='13%'>Total</td><tr>
        <td colspan='5'><hr></td></tr>
        </tr>
        @foreach ($dataTransaksiBarang as $dataBarang)
        <tr><td style='vertical-align:top'>{{ $dataBarang->nama_barang->nama_barang; }}</td>
        <td style='vertical-align:top; text-align:right; padding-right:10px'>{{ number_format($dataBarang->harga_satuan) }}</td>
        <td style='vertical-align:top; text-align:right; padding-right:10px'>{{ number_format($dataBarang->jumlah) }}</td>
        <td style='text-align:right; vertical-align:top'>{{ number_format($dataBarang->harga_satuan * $dataBarang->jumlah) }}</td></tr>
        @endforeach
        <tr>
        <td colspan='4'><hr></td>
        </tr>
        <tr>
        <td colspan = '2'><div style='text-align:right; color:black'>Total : </div></td><td colspan="2" style='text-align:right; font-size:14pt; color:black'>Rp. {{ number_format($dataTransaksi->total_harga) }}</td>
        </tr>
        </table>
        <table style='width:218; font-size:12pt;' cellspacing='2'><tr></br><td align='center'>****** TERIMAKASIH ******</br></td></tr></table></center></body>
        </html>
</body>
</html>