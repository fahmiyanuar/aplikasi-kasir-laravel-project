@extends('layouts.main')

@section('title', 'Manage Produk')

@section('style')
<link href={{ asset("assets/vendor/datatables/dataTables.bootstrap4.min.css") }} rel="stylesheet">
@endsection

@section('content')
<div class="container-fluid">

    <!-- Page Heading -->
    <h1 class="h3 mb-4 text-gray-800">Manage Produk</h1>
    <div class="row">
        <div class="col">
            <div class="card shadow mb-4">
                <div class="card-header py-3">
                    <div class="row">
                        <div class="col d-flex align-items-center">
                            <h6 class="m-0 font-weight-bold text-primary justify-content-start">Data Master Produk</h6>
                        </div>
                        <div class="col d-flex justify-content-end">
                            <button type="button" class="btn btn-sm btn-primary" id="tambah" data-toggle="modal" data-target="#myModal"><i class="fas fa-plus"></i> Tambah Data</button>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-6">
                            <button class="btn btn-primary btn-sm mb-1" type="button" data-toggle="collapse" data-target="#pencarianData" aria-expanded="false" aria-controls="pencarianData">Cari Data</button>
                            <div class="card mb-4" style="border: none">
                                <div class="collapse" id="pencarianData">
                                    <div class="card-body">
                                        <form action="/manage/produk" role="form" method="POST">
                                            {{ csrf_field() }}
                                            <div class="form-group row">
                                                <label for="namaBarang" class="col-sm-4 col-form-label">Nama Barang</label>
                                                <div class="col-sm-8">
                                                    <input type="text" class="form-control" id="namaBarang" name="namaBarang" placeholder="Nama Barang" value="{{ Request::get('namaBarang') }}">
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="harga_min" class="col-sm-4 col-form-label">Total Harga</label>
                                                <div class="col-sm-4">
                                                    <input type="text" class="form-control" id="harga_min" name="harga_min" placeholder="Rp. Harga Min" value="{{ Request::get('harga_min') }}">
                                                </div>
                                                <div class="col-sm-4">
                                                    <input type="text" class="form-control" id="harga_max" name="harga_max" placeholder="Rp. Harga Max" value="{{ Request::get('harga_max') }}">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-sm-8 offset-sm-4">
                                                    <button type="submit" class="btn btn-primary btn-sm"><i class="fas fa-search"></i> Cari</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Nama Barang</th>
                                <th>Harga Satuan</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($data as $result)
                            <tr>
                                <td>{{ ++$no }}</td>
                                <td>{{ $result->nama_barang }}</td>
                                <td>Rp. {{ number_format($result->harga_satuan) }}</td>
                                <td>
                                    <button type="button" class="btn btn-sm btn-warning edit" title="Edit" data-toggle="modal" data-target="#myModal" ref="{{ $result->id }}"><i class="fas fa-edit"></i></button>
                                    <button type="button" class="btn btn-sm btn-danger delete" title="Hapus" data-toggle="modal" data-target="#myModal" ref="{{ $result->id }}"><i class="fas fa-trash-alt"></i></button>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')

    <script src={{ asset("assets/vendor/datatables/jquery.dataTables.min.js") }}></script>
    <script src={{ asset("assets/vendor/datatables/dataTables.bootstrap4.min.js") }}></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script type="text/javascript">
    $(document).ready(function() {
        $('#tambah').click(function() {
            $('#myModal .modal-content').load('{{ url("/manage/produk_tambah") }}');
        });
        $('.edit').click(function() {
            $('#myModal .modal-content').load('{{ url("/manage/produk_edit") }}' + '/' + $(this).attr('ref'));
        });
        $('.delete').click(function() {
            $('#myModal .modal-content').load('{{ url("/manage/produk_delete") }}' + '/' + $(this).attr('ref'));
        });
        $('#dataTable').DataTable({
            "searching": false
        });
    });
    </script>
@endsection