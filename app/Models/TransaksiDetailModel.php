<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TransaksiDetailModel extends Model
{
    protected $table = 'transaksi_pembelian_barang';

    public function nama_barang()
    {
        return $this->belongsTo('App\Models\MasterBarang', 'master_barang_id', 'id');
    }
}
