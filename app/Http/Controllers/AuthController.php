<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
Use Illuminate\Support\Facades\Hash;
Use Illuminate\Support\Facades\Session;
use \App\Models\UserModel;

class AuthController extends Controller
{
    public function login(Request $request)
    {
        if($request->session()->get('LoggedIn')) {
            return redirect('/transaksi');
        }

        return view('pages.login');
    }

    public function login_post(Request $request)
    {
        $login = $request->userEmail;
        $password = $request->userPassword;
        $field_type = filter_var($login, FILTER_VALIDATE_EMAIL) ? 'email' : 'username';

        $data = UserModel::where($field_type, $login)->first();

        if (empty($login) || empty($password)) {
            return redirect('login')->with('alert', 'Username / Email atau Password tidak boleh kosong!');
        }elseif($data) {
            if(Hash::check($password, $data->password)) {
                Session::put('userID', $data->id);
                Session::put('nama', $data->nama);
                Session::put('username', $data->username);
                Session::put('email', $data->email);
                Session::put('role', $data->role);
                Session::put('LoggedIn', true);

                return redirect('/transaksi');
            }else{
                return redirect('login')->with('alert', 'Username/Email atau Password Anda salah!');
            }
        }else{
            return redirect('login')->with('alert', 'Username/Email atau Password Anda salah!');
        }
    }

    public function logout()
    {
        Session::flush();
        return redirect('login')->with('alert','Anda sudah logout');
    }
}
