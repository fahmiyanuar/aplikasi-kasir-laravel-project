<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Carbon;
use App\Models\MasterBarang;
use App\Models\TransaksiModel;
use App\Models\TransaksiDetailModel;
use PDF;

class TransaksiController extends Controller
{
    public function index(Request $request)
    {
        //Jika parameter session LoggedIn tidak ada, maka redirect ke halaman login
        if(!$request->session()->get('LoggedIn')) {
            return redirect('/login');
        }

        $query = TransaksiModel::query();

        if(!$request->input()) {
            $query = $query->orderBy('created_at', 'desc');
        }else{
            
            if(!empty($request->harga_min)) {
                $query = $query->whereBetween('total_harga', [$request->harga_min, $request->harga_max]);
            }

            if(!empty($request->tanggal)) {
                $query = $query->whereDate('created_at', '=', $request->tanggal);
            }

            if(!empty($request->barangs)) {
                $query = $query->select('transaksi_pembelian.*')
                ->join('transaksi_pembelian_barang', 'transaksi_pembelian_barang.transaksi_pembelian_id', '=', 'transaksi_pembelian.id')->whereIn('transaksi_pembelian_barang.master_barang_id', $request->barangs)->distinct();
            }
        }

        if($request->session()->get('role') == 'Kasir') {
            $query = $query->whereDate('created_at', '=', date('Y-m-d'));
        }

        $data = $query->get();
        $dataBarang = MasterBarang::All();

        return view('pages.transaksi', compact('data', 'dataBarang'));
    }

    public function create(Request $request)
    {
        //Jika diakses bukan via AJAX Request
        if(!$request->ajax()){
            exit('No direct script access allowed');
        }

        $data = MasterBarang::All();
        return view('pages.transaksi_tambah', compact('data'));
    }

    public function getharga(Request $request)
    {
        //Jika diakses bukan via AJAX Request
        if(!$request->ajax()){
            exit('No direct script access allowed');
        }

        $id = $request->id;
        $data = MasterBarang::where('id', $id)->pluck('harga_satuan');
        return response()->json($data);	
    }

    public function store(Request $request)
    {
        $dataTransaksi = new TransaksiModel;
        $dataTransaksi->total_harga = $request->total;

        $barang = $request->barang;

        if(empty($request->total) || $request->total == 0) {
            return response()->json(['status' => 'error', 'title' => 'Gagal!', 'message' => 'Gagal menambahkan data!']);
        }elseif($dataTransaksi->save()) {
            $id_transaksi = $dataTransaksi->id;
            foreach($barang as $row => $key) {
                $transaksi_barang = new TransaksiDetailModel;
                $transaksi_barang->transaksi_pembelian_id = $id_transaksi;
                $transaksi_barang->master_barang_id = $barang[$row];
                $transaksi_barang->jumlah = $request->jumlah[$row];
                $transaksi_barang->harga_satuan = $request->harga[$row];
                $transaksi_barang->save();
            }
            return response()->json(['status' => 'success', 'title' => 'Sukses!', 'message' => 'Berhasil menambahkan data!', 'id' => $id_transaksi]);
        }else{
            return response()->json(['status' => 'error', 'title' => 'Gagal!', 'message' => 'Gagal menambahkan data!']);
        }
    }

    public function detail(Request $request, $id)
    {
        $no = 0;
        $dataTransaksi = TransaksiModel::find($id);
        $dataTransaksiBarang = TransaksiDetailModel::where('transaksi_pembelian_id', $id)->get();

        return view('pages.transaksi_detail', compact('dataTransaksi', 'dataTransaksiBarang', 'no'));
    }

    public function print(Request $request, $id)
    {
        $dataTransaksi = TransaksiModel::find($id);
        $dataTransaksiBarang = TransaksiDetailModel::where('transaksi_pembelian_id', $id)->get();

        return view('pages.transaksi_print', compact('dataTransaksi', 'dataTransaksiBarang'));
    }

    public function download(Request $request, $id)
    {
        $no = 0;
        $dataTransaksi = TransaksiModel::find($id);
        $dataTransaksiBarang = TransaksiDetailModel::where('transaksi_pembelian_id', $id)->get();

        $pdf = PDF::loadView('pages.transaksi_download', compact('dataTransaksi', 'dataTransaksiBarang', 'no'));
        return $pdf->download('Detail Transaksi.pdf');

        // return view('pages.transaksi_download', compact('dataTransaksi', 'dataTransaksiBarang', 'no'));
    }
}
