<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\Models\MasterBarang;
use App\Models\UserModel;
use Validator;

class ManageController extends Controller
{
    public function produk_view(Request $request)
    {
        //Jika parameter session LoggedIn tidak ada, maka redirect ke halaman login
        if(!$request->session()->get('LoggedIn')) {
            return redirect('/login');
        }

        //Jika role sebagai Kasir, redirect ke halaman transaksi
        if($request->session()->get('role') == 'Kasir') {
            return redirect('/transaksi');
        }

        //Get all data Master Barang
        $no = 0;
        $query = MasterBarang::query();

        if(!$request->input()) {
            $query = $query->orderBy('id', 'desc');
        }else{

            if(!empty($request->namaBarang)) {
                $query = $query->where('nama_barang', 'like', '%'. $request->namaBarang .'%');
            }

            if(!empty($request->harga_min) && empty($request->harga_max)) {
                $query = $query->where('harga_satuan', $request->harga_min);
            }

            if(!empty($request->harga_min) && !empty($request->harga_max)) {
                $query = $query->whereBetween('harga_satuan', [$request->harga_min, $request->harga_max]);
            }
        }

        $data = $query->get();
        return view('pages.produk', compact('data', 'no'));
    }

    public function produk_add(Request $request)
    {
        //Jika diakses bukan via AJAX Request
        if(!$request->ajax()){
            exit('No direct script access allowed');
        }

        return view('pages.produk_tambah');
    }

    public function produk_store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'nama_barang' => 'required',
            'harga_satuan' => 'required',
        ]);
        
        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()]);			
        }

        $dataStore = new MasterBarang;
        $dataStore->nama_barang = $request->nama_barang;
        $dataStore->harga_satuan = $request->harga_satuan;
        $dataStore->save();

        return response()->json(['success' => 'Berhasil menambahkan data.']);
        
    }

    public function produk_edit(Request $request, $id)
    {
        //Jika diakses bukan via AJAX Request
        if(!$request->ajax()){
            exit('No direct script access allowed');
        }

        $data = MasterBarang::find($id);
        return view('pages.produk_edit', compact('data'));
    }

    public function produk_update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'nama_barang' => 'required',
            'harga_satuan' => 'required',
        ]);
        
        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()]);			
        }

        $dataStore = MasterBarang::find($id);
        $dataStore->nama_barang = $request->nama_barang;
        $dataStore->harga_satuan = $request->harga_satuan;
        $dataStore->update();

        return response()->json(['success' => 'Berhasil update data.']);
        
    }

    public function produk_delete(Request $request, $id)
    {
        //Jika diakses bukan via AJAX Request
        if(!$request->ajax()){
            exit('No direct script access allowed');
        }
        
        $data = MasterBarang::find($id);
        return view('pages.produk_delete', compact('data'));
    }

    public function produk_destroy(Request $request, $id)
    {
        $dataStore = MasterBarang::find($id);
        $dataStore->delete();

        return response()->json(['success' => 'Berhasil delete data.']);
        
    }

    public function pengguna_view(Request $request)
    {
        //Jika parameter session LoggedIn tidak ada, maka redirect ke halaman login
        if(!$request->session()->get('LoggedIn')) {
            return redirect('/login');
        }

        //Jika role sebagai Kasir, redirect ke halaman transaksi
        if($request->session()->get('role') == 'Kasir') {
            return redirect('/transaksi');
        }

        $no = 0;

        if(!$request) {
            $data = UserModel::orderBy('id', 'desc')->get();
        }else{
            $data = UserModel::when($request->userName, function($query, $username) {
                return $query->where('username', 'like', '%'. $username .'%');
            })->when($request->email, function($query, $email) {
                return $query->where('email', 'like', '%'. $email .'%');
            })->when($request->role, function($query, $role) {
                return $query->where('role', $role);
            })->get();
        }

        return view('pages.pengguna', compact('data', 'no'));
    }

    public function pengguna_add(Request $request)
    {
        //Jika diakses bukan via AJAX Request
        if(!$request->ajax()){
            exit('No direct script access allowed');
        }

        return view('pages.pengguna_tambah');
    }

    public function pengguna_store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'nama' => 'required',
            'username' => 'required|alpha_dash|unique:App\Models\UserModel,username',
            'email' => 'required|email|unique:App\Models\UserModel,email',
            'password' => 'required|min:8',
            'role_user' => 'required',
        ]);
        
        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()]);			
        }

        $dataStore = new UserModel;
        $dataStore->nama = $request->nama;
        $dataStore->username = $request->username;
        $dataStore->email = $request->email;
        $dataStore->password = bcrypt($request->password);
        $dataStore->role = $request->role_user;
        $dataStore->save();

        return response()->json(['success' => 'Berhasil menambahkan data.']);
        
    }

    public function pengguna_edit(Request $request, $id)
    {
        //Jika diakses bukan via AJAX Request
        if(!$request->ajax()){
            exit('No direct script access allowed');
        }

        $data = UserModel::find($id);
        return view('pages.pengguna_edit', compact('data'));
    }

    public function pengguna_update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'nama' => 'required',
            'username' => 'required|alpha_dash|unique:App\Models\UserModel,username,'.$id,
            'email' => 'required|email|unique:App\Models\UserModel,email,'.$id,
            'password' => 'nullable|min:8',
            'role_user' => 'required',
        ]);
        
        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()]);			
        }

        $dataStore = UserModel::find($id);
        $dataStore->nama = $request->nama;
        $dataStore->username = $request->username;
        $dataStore->email = $request->email;
        if(!empty($request->password)) {
            $dataStore->password = bcrypt($request->password);
        }
        $dataStore->role = $request->role_user;
        $dataStore->update();

        return response()->json(['success' => 'Berhasil update data.']);
        
    }

    public function pengguna_delete(Request $request, $id)
    {
        //Jika diakses bukan via AJAX Request
        if(!$request->ajax()){
            exit('No direct script access allowed');
        }
        
        $data = UserModel::find($id);
        return view('pages.pengguna_delete', compact('data'));
    }

    public function pengguna_destroy(Request $request, $id)
    {
        $dataStore = UserModel::find($id);
        $dataStore->delete();

        return response()->json(['success' => 'Berhasil delete data.']);
        
    }
}
