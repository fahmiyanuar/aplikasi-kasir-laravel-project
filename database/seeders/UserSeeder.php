<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\UserModel;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        UserModel::insert([
            [
                'id' => 1,
                'nama' => 'User Admin',
                'username' => 'adminuser',
                'email' => 'admin@aplikasikasir.com',
                'password' => bcrypt('12345678'),
                'role' => 'Admin'
            ],
        ]);
    }
}
