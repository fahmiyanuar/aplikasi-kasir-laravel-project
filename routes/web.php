<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ManageController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\TransaksiController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', [TransaksiController::class, 'index']);

//Route Auth
Route::get('login', [AuthController::class, 'login']);
Route::post('login', [AuthController::class, 'login_post']);
Route::get('logout', [AuthController::class, 'logout']);

//Route Manage Produk
Route::get('manage/produk', [ManageController::class, 'produk_view']);
Route::post('manage/produk', [ManageController::class, 'produk_view']);
Route::get('manage/produk_tambah', [ManageController::class, 'produk_add']);
Route::post('manage/produk_tambah', [ManageController::class, 'produk_store'])->name('produk.post');
Route::get('manage/produk_edit/{id}', [ManageController::class, 'produk_edit']);
Route::post('manage/produk_edit/{id}', [ManageController::class, 'produk_update'])->name('produk.update');
Route::get('manage/produk_delete/{id}', [ManageController::class, 'produk_delete']);
Route::post('manage/produk_delete/{id}', [ManageController::class, 'produk_destroy'])->name('produk.delete');

//Route Manage Pengguna
Route::get('manage/pengguna', [ManageController::class, 'pengguna_view']);
Route::post('manage/pengguna', [ManageController::class, 'pengguna_view']);
Route::get('manage/pengguna_tambah', [ManageController::class, 'pengguna_add']);
Route::post('manage/pengguna_tambah', [ManageController::class, 'pengguna_store'])->name('pengguna.post');
Route::get('manage/pengguna_edit/{id}', [ManageController::class, 'pengguna_edit']);
Route::post('manage/pengguna_edit/{id}', [ManageController::class, 'pengguna_update'])->name('pengguna.update');
Route::get('manage/pengguna_delete/{id}', [ManageController::class, 'pengguna_delete']);
Route::post('manage/pengguna_delete/{id}', [ManageController::class, 'pengguna_destroy'])->name('pengguna.delete');

//Route Transaksi
Route::get('transaksi', [TransaksiController::class, 'index']);
Route::post('transaksi', [TransaksiController::class, 'index']);
Route::get('transaksi/tambah', [TransaksiController::class, 'create']);
Route::post('transaksi/tambah', [TransaksiController::class, 'store'])->name('transaksi.post');
Route::get('transaksi/getharga', [TransaksiController::class, 'getharga']);
Route::get('transaksi/detail/{id}', [TransaksiController::class, 'detail']);
Route::get('transaksi/print/{id}', [TransaksiController::class, 'print']);
Route::get('transaksi/download/{id}', [TransaksiController::class, 'download']);